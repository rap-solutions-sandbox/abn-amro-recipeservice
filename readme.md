# Recipe Service
This Project is based on Spring Boot. It publishes a Swagger page with details about the endpoints.
A Postman file is also attached

## Project Requirements:
- Java 17
- Maven 3.x

## Build Instructions

```shell
mvn clean package
```

## Run Instructions
```shell
mvn spring-boot:run
```

The default server will be running on http://localhost:8080 <br>
The Swagger endpoint is available on: http://localhost:8080/swagger-ui/

### Authentication needed for mutations
To mutate data in the recipe service you would need a Token.
```shell
curl --user user:password -XPOST http://localhost:8080/api/v1/token && echo
```

This will return a token object which can be used on recipe API
```json
{
  "token" : "<copy your jwt.token details>"
}
```

### Insert Sample Data
You can import sample recipes from the Spoonacular API. By calling this `http://localhost:8080/api/v1/recipe/samples` POST endpoint. This will import 10 random recipes from Spoonacular.

### Regenerate a private public key
The Token uses private public key encryption to generate / validate a token. 
This section shows you how to generate a new token. In production each environment can have it's own key.
If you run this in the project main directory it will store the files in the correct location. This

```
openssl genpkey -out src/main/resources/app.key -algorithm RSA -pkeyopt rsa_keygen_bits:2048
openssl rsa -in src/main/resources/app.key -pubout -out src/main/resources/app.pub
```


# Future
- Add Flyway support, currently the schema is generate by Hibernate JPA. Which is something you want to manage in a production environment.
- 
