package nl.rapsolutions.abnamro.assessment.recipeservice.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.BasicAuth;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.service.SecurityScheme;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    public static final Contact DEFAULT_CONTACT = new Contact(
            "Ronnie Rap", null, "ronnie@rap-solutions.nl");

    public static final ApiInfo DEFAULT_API_INFO = new ApiInfo(
            "Recipe API's",
            "Allows users to manage their favourite recipes. It allows for adding, updating, removing and fetching recipes. Additionally users should be able to filter available recipes based on one or more criteria",
            "1.0",
            null,
            DEFAULT_CONTACT,
            null, null, List.of());

    private static final Set<String> DEFAULT_PRODUCES_AND_CONSUMES =
            new HashSet<>(List.of("application/json"));

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(DEFAULT_API_INFO)
                .produces(DEFAULT_PRODUCES_AND_CONSUMES)
                .consumes(DEFAULT_PRODUCES_AND_CONSUMES)
                .select().apis(RequestHandlerSelectors.basePackage("nl.rapsolutions.abnamro.assessment"))
                .build();
//                .securityContexts(Arrays.asList(securityContext()))
//                .securitySchemes(Arrays.asList(basicAuthScheme()))
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder()
                .securityReferences(List.of(basicAuthReference()))
                .securityReferences(List.of(basicAuthReference()))
//                .forPaths(PathSelectors.ant("/api/v1/token/**"))
                .build();
    }

    private SecurityScheme basicAuthScheme() {
        return new BasicAuth("BasicAuth");
    }

    private SecurityReference basicAuthReference() {
        return new SecurityReference("BasicAuth", new AuthorizationScope[0]);
    }
}