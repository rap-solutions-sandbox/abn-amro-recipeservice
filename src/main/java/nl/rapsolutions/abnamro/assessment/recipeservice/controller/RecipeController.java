package nl.rapsolutions.abnamro.assessment.recipeservice.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiKeyAuthDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import nl.rapsolutions.abnamro.assessment.recipeservice.entities.Recipe;
import nl.rapsolutions.abnamro.assessment.recipeservice.service.RecipeService;
import nl.rapsolutions.abnamro.assessment.recipeservice.service.SampleService;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.QueryParam;
import java.util.List;

@RestController()
@BasePathAwareController
@Api(value = "Recipe API")
@SecurityScheme(type = SecuritySchemeType.HTTP, name = "bearerAuth", scheme = "bearer", bearerFormat = "JWT")
public class RecipeController {

    public static final String CONTROLLER_BASEPATH = "/api/v1/recipe";

    private final RecipeService recipeService;
    private final SampleService sampleService;

    public RecipeController(RecipeService recipeService, SampleService sampleService) {
        this.recipeService = recipeService;
        this.sampleService = sampleService;
    }

    @GetMapping(CONTROLLER_BASEPATH)
    @Operation(summary = "Get recipes", description = "Get the complete list of recipes")
    public List<Recipe> listRecipes() {
        return recipeService.listRecipes();
    }

    @GetMapping(CONTROLLER_BASEPATH + "/{id}")
    @Operation(summary = "Get a recipe by ID",
            description = "Get the recipe details by a given ID",
            responses = {
                    @ApiResponse(responseCode = "200",
                            description = "The found recipe ",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = Recipe.class))
                    ),
                    @ApiResponse(responseCode = "404",
                            description = "Recipe not found")
            })
    public ResponseEntity<Recipe> getRecipe(@PathVariable("id") Long recipeId) {
        final Recipe recipe = recipeService.getRecipe(recipeId);
        if (recipe == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(recipe);
    }

    // Save operation
    @PostMapping(value = CONTROLLER_BASEPATH)
    @ApiKeyAuthDefinition(key = "X-Auth", in = ApiKeyAuthDefinition.ApiKeyLocation.HEADER, name = "Bearer")
    @Operation(summary = "Create a new recipe",
            description = "Creates a new recipe and returns the created recipe",
            security = {@SecurityRequirement(name = "bearerAuth")},
            responses = {
                    @ApiResponse(responseCode = "200",
                            description = "The created recipe ",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = Recipe.class))
                    )
            })
    public Recipe saveRecipe(@Valid @RequestBody Recipe recipe) {
        return recipeService.createRecipe(recipe);
    }

    @PutMapping(CONTROLLER_BASEPATH + "/{id}")
    @ApiKeyAuthDefinition(key = "X-Auth", in = ApiKeyAuthDefinition.ApiKeyLocation.HEADER, name = "Bearer")
    @Operation(summary = "Update an recipe",
            description = "Updates an existing recipe with the given recipe details",
            security = {@SecurityRequirement(name = "bearerAuth")},
            responses = {
                    @ApiResponse(responseCode = "200",
                            description = "The found recipe ",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = Recipe.class))
                    ),
                    @ApiResponse(responseCode = "404",
                            description = "Recipe not found")
            })
    public ResponseEntity<Recipe> updateRecipe(@PathVariable("id") Long recipeId, @RequestBody Recipe recipe) {
        Recipe updatedRecipe = recipeService.updateRecipe(recipeId, recipe);
        if (updatedRecipe == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(updatedRecipe);
    }

    // Delete operation
    @DeleteMapping(CONTROLLER_BASEPATH + "/{id}")
    @ApiKeyAuthDefinition(key = "X-Auth", in = ApiKeyAuthDefinition.ApiKeyLocation.HEADER, name = "Bearer")
    @Operation(summary = "Delete an recipe by its ID",
            description = "Deletes an recipe by the given ID",
            security = {@SecurityRequirement(name = "bearerAuth")},
            responses = {
                    @ApiResponse(responseCode = "204", description = "When the given recipe is deleted"),
                    @ApiResponse(responseCode = "404", description = "Recipe not found")
            })
    public ResponseEntity<?> deleteRecipeById(@PathVariable("id") Long recipeId) {
        final Recipe recipe = recipeService.getRecipe(recipeId);
        if (recipe == null) {
            return ResponseEntity.notFound().build();
        }
        recipeService.deleteRecipeById(recipeId);
        return ResponseEntity.noContent().build();
    }

    @GetMapping(CONTROLLER_BASEPATH + "/search")
    @Operation(summary = "Search recipes",
            description = "Filter available recipes based on one or more of the following criteria:\n" +
                          "1. Whether or not the dish is vegetarian\n" +
                          "2. The number of servings\n" +
                          "3. Specific ingredients (either include or exclude)\n" +
                          "4. Text search within the instructions.\n" +
                          "For example, this API should be able to handle the following search requests:\n" +
                          "- All vegetarian recipes\n" +
                          "- Recipes that can serve 4 persons and have “potatoes” as an ingredient\n" +
                          "- Recipes without “salmon” as an ingredient that has “oven” in the instructions.\n",
            responses = {
                    @ApiResponse(responseCode = "200", description = "List of recipes based on the given criteria")
            })
    public ResponseEntity<List<Recipe>> searchRecipes(@Parameter(description = "boolean for vegetarian dishes") @QueryParam("vegetarian") Boolean vegetarian,
                                                      @Parameter(description = "minimum number of servings") @QueryParam("minServings") Integer minServings,
                                                      @Parameter(description = "maximum number of servings") @QueryParam("maxServings") Integer maxServings,
                                                      @Parameter(description = "comma seperated list of ingredients to be included in the dish") @QueryParam("includedIngredients") String includedIngredients,
                                                      @Parameter(description = "comma seperated list of ingredients not to be excluded in the dish") @DefaultValue("") @QueryParam("excludedIngredients") String excludedIngredients,
                                                      @Parameter(description = "word to be found in the instructions") @QueryParam("queryInstructions") String queryInstructions) {
        final List<Recipe> recipes = recipeService.findRecipe(vegetarian, minServings, maxServings, includedIngredients != null ? includedIngredients.split(",") : new String[0], excludedIngredients != null ? excludedIngredients.split(",") : new String[0], queryInstructions);
        if (recipes == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(recipes);
    }

    @PostMapping(CONTROLLER_BASEPATH + "/samples")
    @Operation(summary = "Insert sample recipes",
            description = "Calling this will insert 10 random recipes",
            responses = {
                    @ApiResponse(responseCode = "200", description = "List of recipes added")
            })
    public ResponseEntity<List<Recipe>> sampleRecipes() {
        final List<Recipe> newRecipes = sampleService.sampleRecipes(10);
        return ResponseEntity.ok(newRecipes);
    }


}
