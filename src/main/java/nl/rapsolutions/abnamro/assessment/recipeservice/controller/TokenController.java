package nl.rapsolutions.abnamro.assessment.recipeservice.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import nl.rapsolutions.abnamro.assessment.recipeservice.dto.JwtToken;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.jwt.JwtClaimsSet;
import org.springframework.security.oauth2.jwt.JwtEncoder;
import org.springframework.security.oauth2.jwt.JwtEncoderParameters;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import java.time.Instant;
import java.util.stream.Collectors;

@RestController
@BasePathAwareController
@SecurityScheme(type = SecuritySchemeType.HTTP, name = "BasicAuth", scheme = "basic")
public class TokenController {

    public static final String CONTROLLER_BASEPATH = "/api/v1/token";

    @Value("${recipe.app.jwt.expires}")
    private int expires;

    @Value("${recipe.app.jwt.issuer}")
    private String issuer;

    private final JwtEncoder encoder;

    public TokenController(JwtEncoder encoder) {
        this.encoder = encoder;
    }

    @Operation(summary = "Generates a  Token",
            description = "Generates an JWT Token with a specific lifetime. Uses BasicAuth authentication (username/password) to get a token.",
            security = {@SecurityRequirement(name = "BasicAuth")})
    @PostMapping(value = CONTROLLER_BASEPATH)
    public JwtToken token(@ApiIgnore Authentication authentication) {
        Instant now = Instant.now();
        String roles = authentication.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.joining(" "));
        JwtClaimsSet claims = JwtClaimsSet.builder()
                .issuer(issuer)
                .issuedAt(now)
                .expiresAt(now.plusSeconds(expires))
                .subject(authentication.getName())
                .claim("scope", roles)
                .build();
        return new JwtToken(this.encoder.encode(JwtEncoderParameters.from(claims)).getTokenValue());
    }

}
