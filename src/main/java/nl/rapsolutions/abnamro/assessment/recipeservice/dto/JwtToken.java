package nl.rapsolutions.abnamro.assessment.recipeservice.dto;

public record JwtToken(String token) {

}
