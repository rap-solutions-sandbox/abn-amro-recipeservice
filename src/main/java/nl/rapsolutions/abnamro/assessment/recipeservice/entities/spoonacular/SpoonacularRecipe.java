package nl.rapsolutions.abnamro.assessment.recipeservice.entities.spoonacular;

import lombok.Data;

import java.util.List;

@Data
public class SpoonacularRecipe {

    public boolean vegetarian;
    public boolean vegan;
    public boolean glutenfree;
    public boolean dairyFree;
    public boolean veryHealthy;

    public String title;
    public int servings;

    public List<SpooncularIngredient> extendedIngredients;
    public List<SpooncularInstructions> analyzedInstructions;

}
