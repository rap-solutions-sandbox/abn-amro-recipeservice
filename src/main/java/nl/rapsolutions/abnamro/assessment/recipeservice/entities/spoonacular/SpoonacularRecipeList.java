package nl.rapsolutions.abnamro.assessment.recipeservice.entities.spoonacular;

import lombok.Data;

import java.util.List;

@Data
public class SpoonacularRecipeList {
    List<SpoonacularRecipe> recipes;
}
