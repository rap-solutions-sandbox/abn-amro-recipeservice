package nl.rapsolutions.abnamro.assessment.recipeservice.entities.spoonacular;

import lombok.Data;

@Data
public class SpooncularIngredient {
    public String image;
    public String original;
}
