package nl.rapsolutions.abnamro.assessment.recipeservice.entities.spoonacular;

import lombok.Data;

import java.util.List;

@Data
public class SpooncularInstructions {

    List<SpooncularInstructionsStep> steps;
}
