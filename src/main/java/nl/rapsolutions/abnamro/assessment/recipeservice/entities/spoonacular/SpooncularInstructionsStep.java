package nl.rapsolutions.abnamro.assessment.recipeservice.entities.spoonacular;

import lombok.Data;

@Data
public class SpooncularInstructionsStep {
    public int number;
    public String step;
}
