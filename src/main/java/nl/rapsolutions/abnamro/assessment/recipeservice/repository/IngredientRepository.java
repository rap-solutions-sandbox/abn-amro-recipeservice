package nl.rapsolutions.abnamro.assessment.recipeservice.repository;

import nl.rapsolutions.abnamro.assessment.recipeservice.entities.Ingredient;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IngredientRepository extends CrudRepository<Ingredient, Long>, QuerydslPredicateExecutor<Ingredient> {

}
