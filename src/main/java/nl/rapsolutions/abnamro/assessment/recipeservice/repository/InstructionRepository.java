package nl.rapsolutions.abnamro.assessment.recipeservice.repository;

import nl.rapsolutions.abnamro.assessment.recipeservice.entities.Instruction;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InstructionRepository extends CrudRepository<Instruction, Long>, QuerydslPredicateExecutor<Instruction> {

}
