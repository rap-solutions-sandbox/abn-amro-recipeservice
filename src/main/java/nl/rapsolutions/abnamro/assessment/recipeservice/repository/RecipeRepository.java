package nl.rapsolutions.abnamro.assessment.recipeservice.repository;

import nl.rapsolutions.abnamro.assessment.recipeservice.entities.Recipe;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RecipeRepository extends CrudRepository<Recipe, Long>, QuerydslPredicateExecutor<Recipe> {

}
