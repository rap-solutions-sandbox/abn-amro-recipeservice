package nl.rapsolutions.abnamro.assessment.recipeservice.service;

import nl.rapsolutions.abnamro.assessment.recipeservice.entities.Recipe;

import java.util.List;

public interface RecipeService {

    Recipe createRecipe(Recipe recipe);

    // Read
    List<Recipe> listRecipes();

    Recipe getRecipe(Long recipeId);

    Recipe updateRecipe(Long recipeId, Recipe recipe);

    void deleteRecipeById(Long recipeId);

    List<Recipe> findRecipe(Boolean vegetarian, Integer minServings, Integer maxServings, String[] includedIngredients, String[] excludedIngredients, String queryInstructions);
}
