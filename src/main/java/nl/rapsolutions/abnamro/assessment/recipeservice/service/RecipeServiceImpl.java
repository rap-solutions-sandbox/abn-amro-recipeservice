package nl.rapsolutions.abnamro.assessment.recipeservice.service;

import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.JPAExpressions;
import lombok.RequiredArgsConstructor;
import nl.rapsolutions.abnamro.assessment.recipeservice.entities.Ingredient;
import nl.rapsolutions.abnamro.assessment.recipeservice.entities.Instruction;
import nl.rapsolutions.abnamro.assessment.recipeservice.entities.Recipe;
import nl.rapsolutions.abnamro.assessment.recipeservice.repository.IngredientRepository;
import nl.rapsolutions.abnamro.assessment.recipeservice.repository.InstructionRepository;
import nl.rapsolutions.abnamro.assessment.recipeservice.repository.RecipeRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static nl.rapsolutions.abnamro.assessment.recipeservice.entities.QIngredient.ingredient;
import static nl.rapsolutions.abnamro.assessment.recipeservice.entities.QInstruction.instruction;
import static nl.rapsolutions.abnamro.assessment.recipeservice.entities.QRecipe.recipe;

@Service
@RequiredArgsConstructor
public class RecipeServiceImpl implements RecipeService {

    private final RecipeRepository recipeRepository;

    private final InstructionRepository instructionRepository;

    private final IngredientRepository ingredientRepository;

    private final SearchService searchService;

    @Override
    public List<Recipe> listRecipes() {
        return (List<Recipe>) recipeRepository.findAll();
    }

    @Override
    public Recipe getRecipe(Long recipeId) {
        return recipeRepository.findById(recipeId).orElse(null);
    }

    @Override
    public Recipe createRecipe(Recipe recipe) {
        final List<Instruction> instructions = recipe.getInstructions();
        if (instructions != null) {
            instructions.forEach(instruction -> instruction.setRecipe(recipe));
        }
        final List<Ingredient> ingredients = recipe.getIngredients();
        if (ingredients != null) {
            ingredients.forEach(ingredient -> ingredient.setRecipe(recipe));
        }
        return recipeRepository.save(recipe);
    }

    @Override
    public Recipe updateRecipe(Long recipeId, Recipe recipe) {
        Optional<Recipe> recipeFromDB = recipeRepository.findById(recipeId);

        return recipeFromDB.map(r -> {
                    // Update existing item
                    r.setName(recipe.getName());
                    r.setServings(recipe.getServings());
                    r.setVegetarian(recipe.isVegetarian());

                    // Update Lists
                    r.setIngredients(mergeIngredients(r, recipe.getIngredients()));
                    r.setInstructions(mergeInstruction(r, recipe.getInstructions()));

                    return recipeRepository.save(r);
                }).orElse(null);

    }

    private List<Instruction> mergeInstruction(Recipe recipe, List<Instruction> instructionsRequest) {
        List<Instruction> instructions = recipe.getInstructions();

        if (instructionsRequest == null) {
            // Leave the instructions as is
            return instructions;
        }

        // Delete extra items in original List
        while (instructions.size() > instructionsRequest.size()) {
            final int index = instructions.size() - 1;
            final Instruction entity = instructions.get(index);
            instructionRepository.delete(entity);
            instructions.remove(index);
        }

        // Update existing items
        for (int i = 0; i < instructions.size(); i++) {
            final Instruction instruction = instructions.get(i);
            final Instruction instructionFromRequest = instructionsRequest.get(i);

            instruction.setStep(instructionFromRequest.getStep());
            instruction.setDescription(instructionFromRequest.getDescription());
        }

        // Add New items
        if (instructionsRequest.size() > instructions.size()) {
            int startIndex = instructions.size();
            if (startIndex < 0) {
                startIndex = 0;
            }
            final List<Instruction> newInstructions = instructionsRequest.subList(startIndex, instructionsRequest.size());
            instructions.addAll(newInstructions);
            instructions.forEach(instruction -> instruction.setRecipe(recipe));
        }

        return instructions;
    }

    private List<Ingredient> mergeIngredients(Recipe recipe, List<Ingredient> ingredientsRequest) {
        List<Ingredient> ingredients = recipe.getIngredients();

        if (ingredientsRequest == null) {
            // Leave the ingredients as is
            return ingredients;
        }

        // Delete extra items in original List
        while (ingredients.size() > ingredientsRequest.size()) {
            final int index = ingredients.size() - 1;
            final Ingredient entity = ingredients.get(index);
            ingredientRepository.delete(entity);
            ingredients.remove(index);
        }

        // Update existing items
        for (int i = 0; i < ingredients.size(); i++) {
            final Ingredient ingredient = ingredients.get(i);
            final Ingredient ingredientFromRequest = ingredientsRequest.get(i);

            ingredient.setName(ingredientFromRequest.getName());
            ingredient.setImageUrl(ingredientFromRequest.getImageUrl());
        }

        // Add New items
        if (ingredientsRequest.size() > ingredients.size()) {
            int startIndex = ingredients.size();
            if (startIndex < 0) {
                startIndex = 0;
            }
            final List<Ingredient> newIngredients = ingredientsRequest.subList(startIndex, ingredientsRequest.size());
            ingredients.addAll(newIngredients);
            ingredients.forEach(instruction -> instruction.setRecipe(recipe));
        }

        return ingredients;
    }

    @Override
    public void deleteRecipeById(Long recipeId) {
        recipeRepository.deleteById(recipeId);
    }

    @Override
    public List<Recipe> findRecipe(Boolean vegetarian, Integer minServings, Integer maxServings, String[] includedIngredients, String[] excludedIngredients, String queryInstructions) {
        List<BooleanExpression> expressionList = new ArrayList<>();

        // Filter Vegetarian
        if (vegetarian != null) {
            expressionList.add(recipe.vegetarian.eq(vegetarian));
        }

        // Filter Servings
        if (minServings != null && maxServings != null) {
            expressionList.add(recipe.servings.between(minServings, maxServings));
        } else if (minServings != null) {
            expressionList.add(recipe.servings.gt(minServings - 1));
        } else if (maxServings != null) {
            expressionList.add(recipe.servings.lt(maxServings + 1));
        }

        // Ingredients, include
        for (String includeIngredient : includedIngredients) {
            expressionList.add(recipe.in(JPAExpressions.select(ingredient.recipe).from(ingredient).where(ingredient.name.containsIgnoreCase(includeIngredient))));
        }
        for (String excludeIngredient : excludedIngredients) {
            expressionList.add(recipe.notIn(JPAExpressions.select(ingredient.recipe).from(ingredient).where(ingredient.name.containsIgnoreCase(excludeIngredient))));
        }

        // Text Search in instructions
        if (queryInstructions != null) {
            expressionList.add(recipe.in(JPAExpressions.select(instruction.recipe).from(instruction).where(instruction.description.containsIgnoreCase(queryInstructions))));
        }

        // Return result
        return searchService.query(expressionList);
    }
}
