package nl.rapsolutions.abnamro.assessment.recipeservice.service;

import nl.rapsolutions.abnamro.assessment.recipeservice.entities.Recipe;

import java.util.List;

public interface SampleService {
    List<Recipe> sampleRecipes(int amount);
}
