package nl.rapsolutions.abnamro.assessment.recipeservice.service;

import lombok.RequiredArgsConstructor;
import nl.rapsolutions.abnamro.assessment.recipeservice.entities.Ingredient;
import nl.rapsolutions.abnamro.assessment.recipeservice.entities.Instruction;
import nl.rapsolutions.abnamro.assessment.recipeservice.entities.Recipe;
import nl.rapsolutions.abnamro.assessment.recipeservice.entities.spoonacular.SpoonacularRecipe;
import nl.rapsolutions.abnamro.assessment.recipeservice.entities.spoonacular.SpoonacularRecipeList;
import nl.rapsolutions.abnamro.assessment.recipeservice.entities.spoonacular.SpooncularIngredient;
import nl.rapsolutions.abnamro.assessment.recipeservice.entities.spoonacular.SpooncularInstructions;
import nl.rapsolutions.abnamro.assessment.recipeservice.entities.spoonacular.SpooncularInstructionsStep;
import nl.rapsolutions.abnamro.assessment.recipeservice.repository.RecipeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SampleServiceImpl implements SampleService {

    private final RecipeRepository recipeRepository;

    @Autowired
    RestTemplate restTemplate;

    @Value("${recipe.app.spoonacular.apikey}")
    private String apiKey;

    @Override
    public List<Recipe> sampleRecipes(int amount) {
        String spoonacularUrl = "https://api.spoonacular.com/recipes/random?apiKey={key}&limitLicense=true&number={count}";

        SpoonacularRecipeList spoonacularRecipeList = restTemplate.getForObject(spoonacularUrl, SpoonacularRecipeList.class, apiKey, amount);

        if (spoonacularRecipeList == null)
            return Collections.EMPTY_LIST;

        List<Recipe> resultList = new ArrayList();
        for (SpoonacularRecipe recipe : spoonacularRecipeList.getRecipes()) {
            // Convert
            final List<SpooncularIngredient> ingredients = recipe.extendedIngredients;
            final List<SpooncularInstructions> analyzedInstructions = recipe.analyzedInstructions;
            List<SpooncularInstructionsStep> instructionSteps = new ArrayList();
            if (analyzedInstructions != null && analyzedInstructions.size() > 0) {
                instructionSteps = analyzedInstructions.get(0).getSteps();
            }

            final Recipe converted = Recipe.builder()
                    .name(recipe.getTitle())
                    .servings(recipe.getServings())
                    .vegetarian(recipe.isVegetarian())
                    .ingredients(ingredients.stream().map(spooncularIngredient -> Ingredient.builder()
                            .name(spooncularIngredient.getOriginal())
                            .imageUrl("https://spoonacular.com/cdn/ingredients_100x100/" + spooncularIngredient.getImage()).build()).toList())
                    .instructions(instructionSteps.stream().map(spooncularInstructionsStep -> Instruction.builder()
                            .step(spooncularInstructionsStep.getNumber())
                            .description(spooncularInstructionsStep.getStep()).build()).toList())
                    .build();

            final List<Instruction> instructions = converted.getInstructions();
            if (instructions != null) {
                instructions.forEach(instruction -> instruction.setRecipe(converted));
            }
            final List<Ingredient> ingredients1 = converted.getIngredients();
            if (ingredients1 != null) {
                ingredients1.forEach(ingredient -> ingredient.setRecipe(converted));
            }
            resultList.add(recipeRepository.save(converted));

        }
        return resultList;
    }
}
