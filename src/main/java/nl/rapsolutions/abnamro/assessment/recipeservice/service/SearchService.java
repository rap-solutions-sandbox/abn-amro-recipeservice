package nl.rapsolutions.abnamro.assessment.recipeservice.service;

import com.querydsl.core.types.dsl.BooleanExpression;
import nl.rapsolutions.abnamro.assessment.recipeservice.entities.Recipe;

import java.util.List;

public interface SearchService {
    List<Recipe> query(List<BooleanExpression> expressionList);
}
