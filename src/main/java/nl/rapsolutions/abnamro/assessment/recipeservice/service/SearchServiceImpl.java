package nl.rapsolutions.abnamro.assessment.recipeservice.service;

import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.JPQLQueryFactory;
import com.querydsl.jpa.impl.JPAQueryFactory;
import nl.rapsolutions.abnamro.assessment.recipeservice.entities.Recipe;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

import static nl.rapsolutions.abnamro.assessment.recipeservice.entities.QRecipe.recipe;

@Service
public class SearchServiceImpl implements SearchService {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Recipe> query(List<BooleanExpression> expressionList) {
        JPQLQueryFactory queryFactory = new JPAQueryFactory(entityManager);
        JPQLQuery<Recipe> recipeJPQLQuery = queryFactory.selectFrom(recipe);

        final Predicate[] expressions = expressionList.toArray(new Predicate[0]);
        return recipeJPQLQuery.where(expressions).stream().toList();
    }

}
