-- 663832 - 633547
insert into RECIPE (NAME, SERVINGS, VEGETARIAN)
values ('Baked Cinnamon Apple Slices', 6, true);

insert into INGREDIENT (IMAGE_URL, NAME, RECIPE_ID)
values ('https://spoonacular.com/cdn/ingredients_100x100/apple.jpg', '4 Apples Sliced and Peeled – whatever type of apples I have in my refrigerator', 1);
insert into INGREDIENT (IMAGE_URL, NAME, RECIPE_ID)
values ('https://spoonacular.com/cdn/ingredients_100x100/cinnamon.jpg', '1 1/2 tablespoons of Cinnamon', 1);
insert into INGREDIENT (IMAGE_URL, NAME, RECIPE_ID)
values ('https://spoonacular.com/cdn/ingredients_100x100/raisins.jpg', '1/2 cup of Raisins', 1);
insert into INGREDIENT (IMAGE_URL, NAME, RECIPE_ID)
values ('https://spoonacular.com/cdn/ingredients_100x100/raisins.jpg', '2 tablespoons of Earth Balance - melted', 1);

insert into INSTRUCTION (STEP, DESCRIPTION, RECIPE_ID) values (1, 'Mix ingredients together except the raisins.', 1);
insert into INSTRUCTION (STEP, DESCRIPTION, RECIPE_ID) values (2, 'Place in baking dish and in oven at 350 Degrees for 30 minutes.', 1);
insert into INSTRUCTION (STEP, DESCRIPTION, RECIPE_ID) values (3, 'Add raisins the last 5 minutes of baking.', 1);
insert into INSTRUCTION (STEP, DESCRIPTION, RECIPE_ID) values (4, 'Serve and enjoy!Use Organic Ingredients if Available', 1);

-- 662665
insert into RECIPE (NAME, SERVINGS, VEGETARIAN)
values ('Swiss Bircher Muesli', 1, true);

insert into INGREDIENT (IMAGE_URL, NAME, RECIPE_ID)
values ('https://spoonacular.com/cdn/ingredients_100x100/apple.jpg', '1 Apple', 2);
insert into INGREDIENT (IMAGE_URL, NAME, RECIPE_ID)
values ('https://spoonacular.com/cdn/ingredients_100x100/milk.png', '1/2 cup of Milk', 2);
insert into INGREDIENT (IMAGE_URL, NAME, RECIPE_ID)
values ('https://spoonacular.com/cdn/ingredients_100x100/granola.jpg', '1/2 cup muesli', 2);
insert into INGREDIENT (IMAGE_URL, NAME, RECIPE_ID)
values ('https://spoonacular.com/cdn/ingredients_100x100/vanilla-yogurt.png', '3 tablespoons of plain or vanilla yoghurt', 2);

insert into INSTRUCTION (STEP, DESCRIPTION, RECIPE_ID) values (1, 'Put Muesli in a bowl', 2);
insert into INSTRUCTION (STEP, DESCRIPTION, RECIPE_ID) values (2, 'Pour milk to cover the muesli and mix in yoghurt', 2);
insert into INSTRUCTION (STEP, DESCRIPTION, RECIPE_ID) values (3, 'Cover and refigerate for a few hours or overnight', 2);
insert into INSTRUCTION (STEP, DESCRIPTION, RECIPE_ID) values (4, 'When ready to eat, if the mixture is too thick, loosen with milk.Grate apple and stir into the muesli mix', 2);
insert into INSTRUCTION (STEP, DESCRIPTION, RECIPE_ID) values (5, 'Eat', 2);

