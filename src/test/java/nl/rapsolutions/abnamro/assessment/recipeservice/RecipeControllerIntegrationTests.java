package nl.rapsolutions.abnamro.assessment.recipeservice;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import nl.rapsolutions.abnamro.assessment.recipeservice.config.SecurityConfig;
import nl.rapsolutions.abnamro.assessment.recipeservice.entities.Ingredient;
import nl.rapsolutions.abnamro.assessment.recipeservice.entities.Instruction;
import nl.rapsolutions.abnamro.assessment.recipeservice.entities.Recipe;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.stream.Stream;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Import(SecurityConfig.class)
@ExtendWith(MockitoExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@Transactional
@Sql({"/drop_schema.sql", "/create_schema.sql"})
@Sql(value = "/insertRecipes.sql")
public class RecipeControllerIntegrationTests {

    static String validJWTToken = null;

    @Autowired
    MockMvc mvc;

    private final String RECIPE_BASEPATH = "/api/v1/recipe";
    private final String TOKEN_BASEPATH = "/api/v1/token";

    @BeforeEach
    void setUp() throws Exception {
        validJWTToken = getValidToken();
    }

    private String getValidToken() throws Exception {
        MvcResult result = this.mvc.perform(post(TOKEN_BASEPATH)
                        .with(httpBasic("user", "password")))
                .andExpect(status().isOk())
                .andReturn();

        JSONObject jsonToken = new JSONObject(result.getResponse().getContentAsString());
        return jsonToken.getString("token");
    }

    @Test
    void recipeGetAll() throws Exception {
        this.mvc.perform(get(RECIPE_BASEPATH))
                .andExpect(content().json(getAllRecipes()));
    }

    @Test
    void recipeAuthenticatedGetAll() throws Exception {
        this.mvc.perform(get(RECIPE_BASEPATH)
                        .header("Authorization", "Bearer " + validJWTToken))
                .andExpect(content().json(getAllRecipes()));
    }

    @Test
    void recipeGetById() throws Exception {
        this.mvc.perform(get(RECIPE_BASEPATH + "/2"))
                .andExpect(content().json(mapRecipe(recipe2())));
    }

    @Test
    void recipeGetById_NotFound() throws Exception {
        this.mvc.perform(get(RECIPE_BASEPATH + "/10"))
                .andExpect(status().isNotFound());
    }

    @Test
    void recipeAuthenticatedCreateNewRecipe() throws Exception {
        this.mvc.perform(post(RECIPE_BASEPATH)
                .header("Authorization", "Bearer " + validJWTToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapRecipe(Recipe.builder()
                        .name("Recipe 3")
                        .servings(5)
                        .vegetarian(false)
                        .ingredients(Collections.singletonList(Ingredient.builder().name("ingredient new").build()))
                        .build()))
        ).andExpect(content().json(mapRecipe(recipe3())));

        // Check if there is a correct third item
        this.mvc.perform(get(RECIPE_BASEPATH + "/3"))
                .andExpect(content().json(mapRecipe(recipe3())));

    }

    @Test
    void recipeGuestCreateNewRecipe() throws Exception {
        this.mvc.perform(post(RECIPE_BASEPATH).content(
                mapRecipe(Recipe.builder().name("Recipe 3").servings(5).vegetarian(false).build()))
        ).andExpect(status().isUnauthorized());
    }

    @Test
    void recipeAuthenticatedUpdateRecipe() throws Exception {
        final Recipe updatedRecipe = recipe2();
        updatedRecipe.setName("This was Recipe 2");
        updatedRecipe.setServings(8);
        updatedRecipe.setVegetarian(false);

        this.mvc.perform(put(RECIPE_BASEPATH + "/2")
                .header("Authorization", "Bearer " + validJWTToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapRecipe(Recipe.builder().name("This was Recipe 2").servings(8).vegetarian(false).build()))
        ).andExpect(content().json(mapRecipe(updatedRecipe)));
    }

    @Test
    void recipeAuthenticatedUpdateRecipeNotFound() throws Exception {
        this.mvc.perform(put(RECIPE_BASEPATH + "/3")
                .header("Authorization", "Bearer " + validJWTToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapRecipe(Recipe.builder().name("This was Recipe 2").servings(8).vegetarian(false).build()))
        ).andExpect(status().isNotFound());
    }

    @Test
    void recipeGuestUpdateRecipe() throws Exception {
        this.mvc.perform(put(RECIPE_BASEPATH + "/2")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapRecipe(Recipe.builder().name("This was Recipe 2").servings(8).vegetarian(false).build()))
        ).andExpect(status().isUnauthorized());
    }

    @Test
    void recipeAuthenticatedDeleteRecipe() throws Exception {
        this.mvc.perform(delete(RECIPE_BASEPATH + "/2")
                        .header("Authorization", "Bearer " + validJWTToken))
                .andExpect(status().isNoContent());

        // Check if there is only 1 item remaining
        this.mvc.perform(get(RECIPE_BASEPATH))
                .andExpect(content().json(mapRecipe(Collections.singletonList(recipe1()))));
    }

    @Test
    void recipeAuthenticatedDeleteRecipeNotFound() throws Exception {
        this.mvc.perform(delete(RECIPE_BASEPATH + "/3")
                        .header("Authorization", "Bearer " + validJWTToken))
                .andExpect(status().isNotFound());

        // Check if there are still 2 items remaining
        this.mvc.perform(get(RECIPE_BASEPATH))
                .andExpect(content().json(getAllRecipes()));
    }

    @Test
    void recipeGuestDeleteRecipeNotFound() throws Exception {
        this.mvc.perform(put(RECIPE_BASEPATH + "/2"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    void recipeFindRecipe_Vegetarian() throws Exception {
        this.mvc.perform(get(RECIPE_BASEPATH + "/search")
                        .param("vegetarian", "true"))
                .andExpect(content().json(mapRecipe(Collections.singletonList(recipe2()))));
    }

    @Test
    void recipeFindRecipe_Exact4Persons_WithPatatoes() throws Exception {
        this.mvc.perform(get(RECIPE_BASEPATH + "/search")
                        .param("minServings", "4")
                        .param("maxServings", "4")
                        .param("includedIngredients", "potatoes")
                )
                .andExpect(content().json(mapRecipe(Collections.singletonList(recipe1()))));
    }

   @Test
    void recipeFindRecipe_Min4Persons_WithPatatoes() throws Exception {
        this.mvc.perform(get(RECIPE_BASEPATH + "/search")
                        .param("minServings", "4")
                        .param("includedIngredients", "potatoes")
                )
                .andExpect(content().json(mapRecipe(Collections.singletonList(recipe1()))));
    }
   @Test
    void recipeFindRecipe_Max4Persons_WithPatatoes() throws Exception {
        this.mvc.perform(get(RECIPE_BASEPATH + "/search")
                        .param("maxServings", "4")
                        .param("includedIngredients", "potatoes")
                )
                .andExpect(content().json(mapRecipe(Collections.singletonList(recipe1()))));
    }

    @Test
    void recipeFindRecipe_WithoutSalmon_OvenInstructions() throws Exception {
        this.mvc.perform(get(RECIPE_BASEPATH + "/search")
                        .param("excludedIngredients", "salmon")
                        .param("queryInstructions", "oven")
                )
                .andExpect(content().json(mapRecipe(Collections.singletonList(recipe2()))));
    }

    @Test
    void rootWhenGetThen200() throws Exception {
        this.mvc.perform(get("/"))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    void tokenWhenBadCredentialsThen401() throws Exception {
        this.mvc.perform(post("/api/v1/token"))
                .andExpect(status().isUnauthorized());
    }


    private String getAllRecipes() {
        return mapRecipe(Stream.of(recipe1(), recipe2()).toList());
    }

    private Recipe recipe1() {
        return Recipe.builder()
                .id(1L).name("Recipe 1").vegetarian(false).servings(4)
                .ingredients(Stream.of(
                        Ingredient.builder().id(1L).name("4 Apples").imageUrl("apple.jpg").build(),
                        Ingredient.builder().id(2L).name("1 1/2 tablespoons of Cinnamon").imageUrl("cinnamon.jpg").build(),
                        Ingredient.builder().id(3L).name("1/2 part of salmon").imageUrl("salmon.jpg").build(),
                        Ingredient.builder().id(4L).name("2 big potatoes").imageUrl("potatoes.jpg").build()
                ).toList())
                .instructions(Stream.of(
                        Instruction.builder().id(1L).step(1).description("Mix ingredients together except the raisins.").build(),
                        Instruction.builder().id(2L).step(2).description("Place in baking dish and in oven at 350 Degrees for 30 minutes.").build(),
                        Instruction.builder().id(3L).step(3).description("Add raisins the last 5 minutes of baking in the oven.").build(),
                        Instruction.builder().id(4L).step(4).description("Serve and enjoy! Use Organic Ingredients if Available").build()
                ).toList())
                .build();
    }

    private Recipe recipe2() {
        return Recipe.builder()
                .id(2L).name("Recipe 2").vegetarian(true).servings(8)
                .ingredients(Stream.of(
                        Ingredient.builder().id(5L).name("1 Apple").imageUrl("apple.jpg").build(),
                        Ingredient.builder().id(6L).name("1/2 cup of Milk").imageUrl("milk.png").build(),
                        Ingredient.builder().id(7L).name("1/2 cup muesli").imageUrl("granola.jpg").build(),
                        Ingredient.builder().id(8L).name("3 tablespoons of plain or vanilla yoghurt").imageUrl("vanilla-yogurt.png").build()
                ).toList())
                .instructions(Stream.of(
                        Instruction.builder().id(5L).step(1).description("Put Muesli in a bowl").build(),
                        Instruction.builder().id(6L).step(2).description("Pour milk to cover the muesli and mix in yoghurt").build(),
                        Instruction.builder().id(7L).step(3).description("Cover and refigerate for a few hours or overnight").build(),
                        Instruction.builder().id(8L).step(4).description("When ready to eat, if the mixture is too thick, loosen with milk.Grate apple and stir into the muesli mix").build(),
                        Instruction.builder().id(9L).step(5).description("Eat a few minutes after putting it out of the oven").build()
                ).toList())
                .build();

    }

    private Recipe recipe3() {
        return Recipe.builder()
                .id(3L).name("Recipe 3").vegetarian(false).servings(5)
                .ingredients(Collections.singletonList(Ingredient.builder().id(9L).name("ingredient new").build()))
                .build();

    }

    private String mapRecipe(Object recipe) {
        try {
            return new ObjectMapper().writeValueAsString(recipe);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

}
