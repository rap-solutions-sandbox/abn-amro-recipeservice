package nl.rapsolutions.abnamro.assessment.recipeservice.repository;

import nl.rapsolutions.abnamro.assessment.recipeservice.entities.Recipe;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.jdbc.Sql;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@Sql({ "/drop_schema.sql", "/create_schema.sql" })
@Sql("/insertRecipes.sql")
class RecipeRepositoryTest {

    @Autowired
    private RecipeRepository recipeRepository;

    @Test
    void whenInitialized_findAll() {
        Iterable<Recipe> recipes = recipeRepository.findAll();
        assertThat(recipes).hasSize(2);
    }

    @Test
    void whenInitialized_findById() {
        Optional<Recipe> recipe = recipeRepository.findById(2L);
        assertThat(recipe.orElse(null)).isNotNull();
    }

    @Test
    void whenInitialized_findInvalid() {
        Optional<Recipe> recipe = recipeRepository.findById(1000L);
        assertThat(recipe.orElse(null)).isNull();
    }

    @AfterEach
    void cleanup() {
        recipeRepository.deleteAll();
        Iterable<Recipe> recipes = recipeRepository.findAll();
        assertThat(recipes).isEmpty();
    }
}