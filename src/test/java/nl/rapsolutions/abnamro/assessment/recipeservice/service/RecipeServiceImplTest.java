package nl.rapsolutions.abnamro.assessment.recipeservice.service;

import com.querydsl.core.types.dsl.BooleanExpression;
import nl.rapsolutions.abnamro.assessment.recipeservice.entities.Ingredient;
import nl.rapsolutions.abnamro.assessment.recipeservice.entities.Instruction;
import nl.rapsolutions.abnamro.assessment.recipeservice.entities.Recipe;
import nl.rapsolutions.abnamro.assessment.recipeservice.repository.IngredientRepository;
import nl.rapsolutions.abnamro.assessment.recipeservice.repository.InstructionRepository;
import nl.rapsolutions.abnamro.assessment.recipeservice.repository.RecipeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class RecipeServiceImplTest {

    @Mock
    private RecipeRepository recipeRepository;
    @Mock
    private InstructionRepository instructionRepository;
    @Mock
    private IngredientRepository ingredientRepository;
    @Mock
    private SearchService searchService;

    @InjectMocks
    private RecipeServiceImpl recipeService;

    private List<Recipe> recipeList = null;

    @Captor
    ArgumentCaptor<List<BooleanExpression>> expressionCaptor;

    @BeforeEach
    void setUp() {
        recipeList = Stream.of(
                new Recipe(1L, "Test 1", false, 5, new ArrayList<>(), new ArrayList<>()),
                new Recipe(2L, "Test 2", true, 10, new ArrayList<>(), new ArrayList<>())
        ).toList();

        // Enrich
        final Recipe recipe1 = recipeList.get(0);
        recipe1.getIngredients().add(new Ingredient(10L, recipe1, "Cheese", "cheese.png"));
        recipe1.getIngredients().add(new Ingredient(11L, recipe1, "Tomato", "tomato.png"));
        recipe1.getInstructions().add(new Instruction(10L, recipe1, 1, "Slice cheese"));
        recipe1.getInstructions().add(new Instruction(11L, recipe1, 2, "Put tomato on cheese"));

        final Recipe recipe2 = recipeList.get(1);
        recipe2.getIngredients().add(new Ingredient(20L, recipe2, "Hamburger", "hamburger.png"));
        recipe2.getIngredients().add(new Ingredient(21L, recipe2, "Unions", "unions.png"));
        recipe2.getInstructions().add(new Instruction(20L, recipe2, 1, "Bake hamburger and unions"));
        recipe2.getInstructions().add(new Instruction(21L, recipe2, 2, "Put on bread"));
    }

    @Test
    void listRecipes() {
        // Given
        when(recipeRepository.findAll()).thenReturn(recipeList);

        // When
        final List<Recipe> recipes = recipeService.listRecipes();

        // Then
        assertThat(recipes).hasSize(2);

        verifyRecipe(recipeList.get(0), recipes.get(0));
        verifyRecipe(recipeList.get(1), recipes.get(1));
    }

    private void verifyRecipe(Recipe expected, Recipe recipe) {
        assertThat(recipe).isEqualTo(expected);
        assertThat(recipe.getName()).isEqualTo(expected.getName());
        assertThat(recipe.isVegetarian()).isEqualTo(expected.isVegetarian());
        assertThat(recipe.getServings()).isEqualTo(expected.getServings());
    }

    @Test
    void getRecipe1() {
        // Given
        when(recipeRepository.findById(1L)).thenReturn(Optional.ofNullable(recipeList.get(0)));

        // When
        final Recipe recipe = recipeService.getRecipe(1L);

        // Then
        assertThat(recipe).isNotNull();
        verifyRecipe(recipeList.get(0), recipe);
    }

    @Test
    void getRecipe2() {
        // Given
        when(recipeRepository.findById(2L)).thenReturn(Optional.ofNullable(recipeList.get(1)));

        // When
        final Recipe recipe = recipeService.getRecipe(2L);

        // Then
        assertThat(recipe).isNotNull();
        verifyRecipe(recipeList.get(1), recipe);
    }

    @Test
    void getRecipeNotExists() {
        // Given
        when(recipeRepository.findById(1000L)).thenReturn(Optional.empty());

        // When
        final Recipe recipe = recipeService.getRecipe(1000L);

        // Then
        assertThat(recipe).isNull();
    }

    @Test
    void createRecipe() {
        // Given
        when(recipeRepository.save(recipe())).thenReturn(recipe());

        // When
        final Recipe recipe = recipeService.createRecipe(recipe());

        // Then
        assertThat(recipe).isNotNull();
        verify(recipeRepository).save(recipe);
    }

    private Recipe recipe() {
        return Recipe.builder()
                .id(10L)
                .name("Created Recipe")
                .servings(15)
                .build();
    }

    @Test
    void updateRecipeSimple() {
        // Given
        final Recipe recipeToUpdate = Recipe.builder().name("This updated 2").servings(8).vegetarian(false).build();

        final Recipe recipeSaved = Recipe.builder().name("This updated 2").servings(8).vegetarian(false).build();
        recipeSaved.setIngredients(recipeList.get(1).getIngredients());
        recipeSaved.setInstructions(recipeList.get(1).getInstructions());

        when(recipeRepository.findById(2L)).thenReturn(Optional.ofNullable(recipeList.get(1)));
        when(recipeRepository.save(recipeList.get(1))).thenReturn(recipeSaved);

        // When
        final Recipe updatedRecipe = recipeService.updateRecipe(2L, recipeToUpdate);

        // Then
        assertThat(updatedRecipe).isNotNull();
        assertThat(updatedRecipe.getName()).isEqualTo("This updated 2");
        assertThat(updatedRecipe.getServings()).isEqualTo(8);
        assertThat(updatedRecipe.isVegetarian()).isFalse();

        // Ingredients and Instructions untouched
        assertThat(updatedRecipe.getIngredients()).hasSize(2);
        assertThat(updatedRecipe.getInstructions()).hasSize(2);
    }

    @Test
    void updateRecipe_NonExisting() {
        // Given
        final Recipe recipeToUpdate = Recipe.builder().name("This updated 2").servings(8).vegetarian(false).build();
        when(recipeRepository.findById(100L)).thenReturn(Optional.empty());

        // When
        final Recipe updatedRecipe = recipeService.updateRecipe(100L, recipeToUpdate);

        // Then
        assertThat(updatedRecipe).isNull();
    }

    @Test
    void updateRecipe_ReduceTo_One_InstructionAndIngredient() {
        // Given
        final Recipe recipeToUpdate = Recipe.builder()
                .name("This updated 2").servings(8).vegetarian(false)
                .ingredients(Collections.singletonList(Ingredient.builder().name("Bread").imageUrl("bread.png").build()))
                .instructions(Collections.singletonList(Instruction.builder().step(1).description("Eat bread").build()))
                .build();

        final Ingredient deletedIngredient = recipeList.get(1).getIngredients().get(1);
        final Instruction deletedInstruction = recipeList.get(1).getInstructions().get(1);

        when(recipeRepository.findById(2L)).thenReturn(Optional.ofNullable(recipeList.get(1)));
        when(recipeRepository.save(recipeList.get(1))).thenReturn(recipeToUpdate);

        // When
        final Recipe updatedRecipe = recipeService.updateRecipe(2L, recipeToUpdate);

        // Then
        assertThat(updatedRecipe).isNotNull();
        assertThat(updatedRecipe.getName()).isEqualTo("This updated 2");
        assertThat(updatedRecipe.getServings()).isEqualTo(8);
        assertThat(updatedRecipe.isVegetarian()).isFalse();

        // Ingredients and Instructions are reduced to 1
        assertThat(updatedRecipe.getIngredients()).hasSize(1);
        assertThat(updatedRecipe.getInstructions()).hasSize(1);

        verify(ingredientRepository).delete(deletedIngredient);
        verify(instructionRepository).delete(deletedInstruction);
    }

    @Test
    void updateRecipe_IncreaseTo_Three_InstructionAndIngredient() {
        // Given
        final Recipe recipeToUpdate = Recipe.builder()
                .name("This updated 2").servings(8).vegetarian(false)
                .build();
        recipeToUpdate.setIngredients(Stream.of(
                Ingredient.builder().name("Bread").imageUrl("bread.png").build(),
                Ingredient.builder().name("Hamburger").imageUrl("hamburger.png").build(),
                Ingredient.builder().name("Unions").imageUrl("unions.png").build()
        ).toList());
        recipeToUpdate.setInstructions(Stream.of(
                Instruction.builder().step(1).description("Bake hamburger").build(),
                Instruction.builder().step(2).description("Caramel the unions").build(),
                Instruction.builder().step(3).description("Put on bread").build()
        ).toList());

        when(recipeRepository.findById(2L)).thenReturn(Optional.ofNullable(recipeList.get(1)));
        when(recipeRepository.save(recipeList.get(1))).thenReturn(recipeToUpdate);

        // When
        final Recipe updatedRecipe = recipeService.updateRecipe(2L, recipeToUpdate);

        // Then
        assertThat(updatedRecipe).isNotNull();
        assertThat(updatedRecipe.getName()).isEqualTo("This updated 2");
        assertThat(updatedRecipe.getServings()).isEqualTo(8);
        assertThat(updatedRecipe.isVegetarian()).isFalse();

        // Ingredients and Instructions are increased to 3
        assertThat(updatedRecipe.getIngredients()).hasSize(3);
        assertThat(updatedRecipe.getInstructions()).hasSize(3);

    }


    @Test
    void deleteRecipeById() {
        // Given
        // When
        recipeService.deleteRecipeById(1L);

        // Then
        verify(recipeRepository).deleteById(1L);
    }

    @Test
    void findRecipe() {
        // Given no arguments
        // When
        recipeService.findRecipe(null, null, null, new String[0], new String[0], null);

        // Then
        verify(searchService).query(expressionCaptor.capture());
        assertThat(expressionCaptor.getValue()).isEmpty();
    }

    @Test
    void findRecipe_Vegetarian() {
        // Given vegetarian argument
        // When
        recipeService.findRecipe(Boolean.FALSE, null, null, new String[0], new String[0], null);

        // Then
        verify(searchService).query(expressionCaptor.capture());
        final List<BooleanExpression> search2 = expressionCaptor.getValue();
        assertThat(search2).hasSize(1);
        assertThat(search2.get(0)).hasToString("recipe.vegetarian = false");
    }

    @Test
    void findRecipe_RangedServings() {
        // Given servings
        // When
        recipeService.findRecipe(null, 4, 12, new String[0], new String[0], null);

        // Then
        verify(searchService).query(expressionCaptor.capture());
        final List<BooleanExpression> search3 = expressionCaptor.getValue();
        assertThat(search3).hasSize(1);
        assertThat(search3.get(0)).hasToString("recipe.servings between 4 and 12");
    }

    @Test
    void findRecipe_MinimumServings() {
        // Given minimum servings
        // When
        recipeService.findRecipe(null, 4, null, new String[0], new String[0], null);

        // Then
        verify(searchService).query(expressionCaptor.capture());
        final List<BooleanExpression> search3a = expressionCaptor.getValue();
        assertThat(search3a).hasSize(1);
        assertThat(search3a.get(0)).hasToString("recipe.servings > 3");
    }

    @Test
    void findRecipe_MaxServings() {
        // Given maximum servings
        // When
        recipeService.findRecipe(null, null, 12, new String[0], new String[0], null);

        // Then
        verify(searchService).query(expressionCaptor.capture());
        final List<BooleanExpression> search3b = expressionCaptor.getValue();
        assertThat(search3b).hasSize(1);
        assertThat(search3b.get(0)).hasToString("recipe.servings < 13");
    }

    @Test
    void findRecipe_IncludedIngredients() {
        // Given included ingredients
        // When
        recipeService.findRecipe(null, null, null, new String[] { "cheese" }, new String[0], null);

        // Then
        verify(searchService).query(expressionCaptor.capture());
        final List<BooleanExpression> search4 = expressionCaptor.getValue();
        assertThat(search4).hasSize(1);
        assertThat(search4.get(0).toString()).startsWith("recipe in com.querydsl.core.DefaultQueryMetadata");
    }

    @Test
    void findRecipe_ExcludedIngredients() {
        // Given excluded ingredients
        // When
        recipeService.findRecipe(null, null, null, new String[0], new String[] { "tomato" }, null);

        // Then
        verify(searchService).query(expressionCaptor.capture());
        final List<BooleanExpression> search5 = expressionCaptor.getValue();
        assertThat(search5).hasSize(1);
        assertThat(search5.get(0).toString()).startsWith("recipe not in com.querydsl.core.DefaultQueryMetadata");
    }

    @Test
    void findRecipe_Instruction() {
        // Given instructions
        // When
        recipeService.findRecipe(null, null, null, new String[0],  new String[0], "enjoy");

        // Then
        verify(searchService).query(expressionCaptor.capture());
        final List<BooleanExpression> search6 = expressionCaptor.getValue();
        assertThat(search6).hasSize(1);
        assertThat(search6.get(0).toString()).startsWith("recipe in com.querydsl.core.DefaultQueryMetadata");
    }
}