CREATE TABLE ingredient
(
    id        BIGINT AUTO_INCREMENT NOT NULL,
    recipe_id BIGINT,
    name      VARCHAR(255),
    image_url VARCHAR(255),
    CONSTRAINT pk_ingredient PRIMARY KEY (id)
);

CREATE TABLE instruction
(
    id          BIGINT AUTO_INCREMENT NOT NULL,
    recipe_id   BIGINT,
    step        INT                   NOT NULL,
    description VARCHAR(255),
    CONSTRAINT pk_instruction PRIMARY KEY (id)
);

CREATE TABLE recipe
(
    id         BIGINT AUTO_INCREMENT NOT NULL,
    name       VARCHAR(255),
    vegetarian BOOLEAN               NOT NULL,
    servings   INT                   NOT NULL,
    CONSTRAINT pk_recipe PRIMARY KEY (id)
);

ALTER TABLE ingredient
    ADD CONSTRAINT FK_INGREDIENT_ON_RECIPE FOREIGN KEY (recipe_id) REFERENCES recipe (id);

ALTER TABLE instruction
    ADD CONSTRAINT FK_INSTRUCTION_ON_RECIPE FOREIGN KEY (recipe_id) REFERENCES recipe (id);