-- 663832 - 633547
insert into RECIPE (NAME, SERVINGS, VEGETARIAN)
values ('Recipe 1', 4, false);

insert into INGREDIENT (IMAGE_URL, NAME, RECIPE_ID)
values ('apple.jpg', '4 Apples', 1);
insert into INGREDIENT (IMAGE_URL, NAME, RECIPE_ID)
values ('cinnamon.jpg', '1 1/2 tablespoons of Cinnamon', 1);
insert into INGREDIENT (IMAGE_URL, NAME, RECIPE_ID)
values ('salmon.jpg', '1/2 part of salmon', 1);
insert into INGREDIENT (IMAGE_URL, NAME, RECIPE_ID)
values ('potatoes.jpg', '2 big potatoes', 1);

insert into INSTRUCTION (STEP, DESCRIPTION, RECIPE_ID) values (1, 'Mix ingredients together except the raisins.', 1);
insert into INSTRUCTION (STEP, DESCRIPTION, RECIPE_ID) values (2, 'Place in baking dish and in oven at 350 Degrees for 30 minutes.', 1);
insert into INSTRUCTION (STEP, DESCRIPTION, RECIPE_ID) values (3, 'Add raisins the last 5 minutes of baking in the oven.', 1);
insert into INSTRUCTION (STEP, DESCRIPTION, RECIPE_ID) values (4, 'Serve and enjoy! Use Organic Ingredients if Available', 1);


-- 662665
insert into RECIPE (NAME, SERVINGS, VEGETARIAN)
values ('Recipe 2', 8, true);

insert into INGREDIENT (IMAGE_URL, NAME, RECIPE_ID)
values ('apple.jpg', '1 Apple', 2);
insert into INGREDIENT (IMAGE_URL, NAME, RECIPE_ID)
values ('milk.png', '1/2 cup of Milk', 2);
insert into INGREDIENT (IMAGE_URL, NAME, RECIPE_ID)
values ('granola.jpg', '1/2 cup muesli', 2);
insert into INGREDIENT (IMAGE_URL, NAME, RECIPE_ID)
values ('vanilla-yogurt.png', '3 tablespoons of plain or vanilla yoghurt', 2);

insert into INSTRUCTION (STEP, DESCRIPTION, RECIPE_ID) values (1, 'Put Muesli in a bowl', 2);
insert into INSTRUCTION (STEP, DESCRIPTION, RECIPE_ID) values (2, 'Pour milk to cover the muesli and mix in yoghurt', 2);
insert into INSTRUCTION (STEP, DESCRIPTION, RECIPE_ID) values (3, 'Cover and refigerate for a few hours or overnight', 2);
insert into INSTRUCTION (STEP, DESCRIPTION, RECIPE_ID) values (4, 'When ready to eat, if the mixture is too thick, loosen with milk.Grate apple and stir into the muesli mix', 2);
insert into INSTRUCTION (STEP, DESCRIPTION, RECIPE_ID) values (5, 'Eat a few minutes after putting it out of the oven', 2);

